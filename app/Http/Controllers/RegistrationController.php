<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use Illuminate\Http\Request;
use App\User;

class RegistrationController extends Controller
{
    public function create()
    {
        return view('register');
    }

    public function store(RegistrationRequest $request)
    {
        $data = User::register($request);
        if ($data == false) {
            return redirect()->back()->with('status', 'Name already exist in database');
        } else {
            return redirect()->to('/index');
        }
    }
}
