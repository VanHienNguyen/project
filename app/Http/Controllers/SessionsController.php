<?php

namespace App\Http\Controllers;

use App\Http\Requests\SessionRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class SessionsController extends Controller
{
    public function login()
    {
        return view('/login');
    }

    public function store(SessionRequest $request)
    {
        $result = User::login($request);
        if ($result == false) {
            return back()->withErrors([
                'message' => 'Password or Username is incorrect'
            ]);
        } else {
            return redirect()->to('/index');
        }
    }

    public function destroy()
    {
        User::exit();
        return redirect()->to('/index');
    }
}
