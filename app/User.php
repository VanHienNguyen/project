<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Redirect, Response;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'role', 'email', 'password'
    ];

    protected $table = 'users';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     *Add a mutator to ensure hashed passwords (need to protect password):
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function post()
    {
        return $this->hasMany('App\Post', 'user_id', 'id');
    }

    public function feedback()
    {
        return $this->hasMany('App\FeedBack', 'user_id', 'id');
    }
    /**
     * Checks if User has access to $permissions.
     */

    public static function index()
    {
        $result['users'] = User::orderBy('id', 'desc')->paginate(8);
        return $result;
    }

    public static function store($request)
    {
        $userId = $request->user_id;
        $result = User::updateOrCreate(
            ['id' => $userId],
            [
                'name' => $request->name,
                'email' => $request->email,
                'role' => $request->role,
                'password' => $request->password
            ]
        );
        return $result;
    }

    public static function showPosts($request)
    {
        if ($request->ajax()) {
            $user_id = $request->get('id');
            $result = User::findOrFail($user_id)->post()->get();
            return $result;
        }
    }

    public static function edit($id)
    {
        $where = array('id' => $id);
        $result  = User::where($where)->first();
        return $result;
    }

    public static function remove($id)
    {
        $result = User::where('id', $id)->delete();
        return $result;
    }

    public static function search($request)
    {
        if ($request->ajax()) {
            $search = $request->get('search');
            $result = User::where('name', 'LIKE', '%' . $search . '%')->get();
            return $result;
        }
    }

    public static function login($request)
    {
        $user = $request->only('name', 'password');
        $result = auth()->attempt($user);
        return $result;
    }

    public static function exit()
    {
        auth()->logout();
    }

    public static function register($request)
    {
        $input = $request->all();
        if (User::where('name', $request->name)) {
            return false;
        } else {
            $user = User::create($input);
            auth()->login($user);
        }
    }
}
