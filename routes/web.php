<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('index', function () {
    return view('index');
});

Route::get('/register', 'RegistrationController@create');
Route::post('register', 'RegistrationController@store');

Route::get('/login', 'SessionsController@login')->middleware('checklogout');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');

Route::get('/index', 'IndexController@index');
Route::post('/index/search', 'IndexController@search');

Route::post('/index/store', 'IndexController@store');

Route::post('/admin/search', 'AdminController@search');
Route::post('/admin/show', 'AdminController@show');

Route::post('/post/search', 'PostController@search');

Route::group(['middleware' => 'checklogin'], function () {
    Route::resource('/admin', 'AdminController');
    Route::resource('/post', 'PostController');
});
